<p align="center">
    <a href="https://sulu.io/" target="_blank">
        <img width="50%" src="https://sulu.io/website/images/sulu.svg" alt="Sulu logo">
    </a>
</p>

<p align="center">
    <a href="https://github.com/sulu/skeleton/blob/master/LICENSE" target="_blank">
        <img src="https://img.shields.io/github/license/sulu/skeleton.svg" alt="GitHub license">
    </a>
    <a href="https://github.com/sulu/skeleton/releases" target="_blank">
        <img src="https://img.shields.io/github/tag/sulu/skeleton.svg" alt="GitHub tag (latest SemVer)">
    </a>
    <a href="https://travis-ci.org/sulu/skeleton" target="_blank">
        <img src="https://img.shields.io/travis/sulu/skeleton.svg?label=travis" alt="Travis build">
    </a>
</p>
[Sulu](https://sulu.io/) es un sistema de gestión de contenido PHP **de código abierto altamente extensible basado** en el Framework [Symfony](https://symfony.com/). Sulu está desarrollado para ofrecer sitios web robustos **multilingües y de múltiples portales** a la vez que proporciona una **interfaz de administración intuitiva y extensible** para administrar el ciclo de vida completo del contenido.

Eche un vistazo al [sitio web oficial de Sulu](https://sulu.io/) para obtener una lista completa de las características, valores fundamentales y casos de uso de Sulus.

<p align="center">
    <img width="80%" src="https://sulu.io/uploads/media/800x@2x/07/167-ezgif.gif?v=2" alt="Sulu Slideshow">
</p>

Este repositorio proporciona una **estructura basica de proyecto recomendada para generar sistemas basados en el sistema de administración de contenido de Sulu**.
Además, requiere y configura el marco central del sistema de administración de contenido de Sulu [sulu/sulu](https://github.com/sulu/sulu) como tambien [sulu/article-bundle](https://github.com/sulu/SuluArticleBundle)

## Instalacion y Documentacion

1. clone el repositorio
2. modifique los datos necesarios del archivo .env
3. agregue el valor de PROJECT_DOMAIN que haya definido en .env y realizar asignacion del dominio. en caso de entorno local y en linux agregar en /etc/hosts
```bash
#como root
echo -n "127.0.0.1       sulu.localhost" >> /etc/hosts
```
4. parado dentro de la carpeta del proyecto ejectuar
```bash
docker-compose up  # agregar -d para ejectuarlo en segundo plano
```

visite la [documentación de Sulu Oficial](http://docs.sulu.io/en/latest/book/getting-started.html) para descubrir **mas informacion, configuraciones y estructura del proyecto** para personalizar el sistema  a sus necesidades específicas .


## ❤️&nbsp; Comunidad

El sistema de gestión de contenido de Sulu es un **proyecto de código abierto impulsado por la comunidad** respaldado por varias empresas asociadas. Estamos comprometidos con un proceso de desarrollo totalmente transparente y **nos gusta mucho contribuir con este proyecto**. 

## ✅&nbsp; Requerimientos

Es necesario tener instalado docker y docker-compose para ejectuar el proyecto de manera local en entorno de desarrollo.
